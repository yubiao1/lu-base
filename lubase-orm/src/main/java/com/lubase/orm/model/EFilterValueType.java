package com.lubase.orm.model;

/**
 * TableFilter filterValue 值类型
 */
public enum EFilterValueType {
    /**
     * 静态值
     */
    Fix,
    /**
     * 宏变量
     */
    Macro,
    /**
     * 表单值
     */
    Form
}
