package com.lubase.orm.model.statistics;

import lombok.Data;

@Data
public class StatisticsEntity {
    private String r;
    private String c;
    private Double v;
}
