package com.lubase.orm.constant;

public class CommonConst {
    /**
     * 关联字段中间分隔符号
     */
    public static final String REF_FIELD_SEPARATOR = "#";


    /**
     * 主库数据源Id
     */
    public static final String MAIN_DATABASE_ID = "0";
}
