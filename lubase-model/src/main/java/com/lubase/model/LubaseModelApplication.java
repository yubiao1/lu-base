package com.lubase.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LubaseModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubaseModelApplication.class, args);
    }

}
