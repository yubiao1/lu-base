package com.lubase.model.constrant;

/**
 * 宏变量前缀
 */
public class MacroCost {
    /**
     * 客户端宏变量前缀
     */
    public static final String clientMacroPre = "@@C.";
    /**
     * 服务端宏变量前缀
     */
    public static final String serverMacroPre = "@@S.";
}
