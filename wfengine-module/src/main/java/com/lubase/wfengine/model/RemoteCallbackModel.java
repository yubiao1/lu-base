package com.lubase.wfengine.model;

import lombok.Data;

@Data
public class RemoteCallbackModel {
    private Integer success;
    private String message;
}
