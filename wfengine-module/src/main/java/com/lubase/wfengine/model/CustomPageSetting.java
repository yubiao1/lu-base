package com.lubase.wfengine.model;

import lombok.Data;

@Data
public class CustomPageSetting {
    private String navCode;
}
