package com.lubase.wfengine.model;

import lombok.Data;

@Data
public class OperatorUserModel {
    private String userId;
    private String userName;
}
