package com.lubase.wfengine.model;

import lombok.Data;

@Data
public class TransferInfoModel {
    private String oInsId;
    private String displayName;
}
