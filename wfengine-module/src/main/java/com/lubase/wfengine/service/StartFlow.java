package com.lubase.wfengine.service;

public interface StartFlow {
    Integer startWfByApi(String serviceId, String dataId, String userId);
}
