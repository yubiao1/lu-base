package com.lubase.core.model.customForm;

import lombok.Data;

@Data
public class FieldBackfillModel {
    private String field;
    private String backfillRules;
}
