package com.lubase.core.model.customForm;

import lombok.Data;

import java.util.List;

/**
 * 表单 Tabs 数据对象
 *
 * @author bluesky
 */
@Data
public class Tabs {

    private List<FormTab> tabs;
}
