package com.lubase.core.model.customForm;

import lombok.Data;

@Data
public class FieldControlModel {
    private String field;
    private String controlRules;
}
