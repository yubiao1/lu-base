package com.lubase.core.model;

import lombok.Data;

/**
 * 页面表格配置信息
 */
@Data
public class PageGridInfoModel {
    /**
     * 可编辑列
     */
    private String editColumns;
}
