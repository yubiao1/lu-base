package com.lubase.core.model.customForm;

import lombok.Data;

@Data
public class FieldRelyModel {
    private String field;
    private String relyFields;
}
