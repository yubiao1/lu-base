package com.lubase.core.service.multiApplications.impl;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessControlContext;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.any;

/**
 * 模块的ClassLoader，继承自URLClassLoader，同时可以强制指定一些包下的class，由本ClassLoader自己加载，不通过父ClassLoader加载，突破双亲委派机制。
 *
 * @author cgm
 *
 */
public class AppClassLoader extends URLClassLoader {


    /* The context to be used when loading classes and resources */
    private AccessControlContext accessControlContext;
    private static Logger logger = LoggerFactory.getLogger(AppClassLoader.class);

    /** java的包必须排除，避免安全隐患 */
    public static final String[] DEFAULT_EXCLUDED_PACKAGES = new String[] {"java.", "javax.", "sun.", "oracle."};

    /** 需要排除的包 */
    private final Set<String> excludedPackages = new HashSet<String>();

    /** 需要子加载器优先加载的包 */
    private final List<String> overridePackages;

    public AppClassLoader(List<URL> urls, ClassLoader parent, List<String> overridePackages) {
        super(urls.toArray(new URL[] {}), parent);
        this.overridePackages = overridePackages;
        this.excludedPackages.addAll(Sets.newHashSet(DEFAULT_EXCLUDED_PACKAGES));
    }

    /**
     * 覆盖双亲委派机制
     *
     * @see ClassLoader#loadClass(String, boolean)
     */
    protected Set<Class<?>> loadClassByPack(String name, List<URL> urls) throws ClassNotFoundException {
        Set<Class<?>> result = null;
        synchronized (AppClassLoader.class) {
                result = findControllerByPack(name, urls);

        }
        //使用默认类加载方式
        return result;

    }
    protected Set<Class<?>> findControllerByPack(final String pack, List<URL> urls)
    {
        // 第一个class类的集合
        Set<Class<?>> classes = new LinkedHashSet<Class<?>>();
        // 是否循环迭代
        boolean recursive = true;
        // 获取包的名字 并进行替换
        String packageName = pack;
        String packageDirName = packageName.replace('.', '/');
        // 循环迭代下去
        for (URL url: urls)  {
            // 获取下一个元素
            logger.info(url.toString());
            JarFile jar;
            try {
                // 获取jar
                jar = ((JarURLConnection) url.openConnection()).getJarFile();
                // 从此jar包 得到一个枚举类
                Enumeration<JarEntry> entries = jar.entries();
                // 同样的进行循环迭代
                while (entries.hasMoreElements()) {
                    // 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
                    JarEntry entry = entries.nextElement();
                    String name = entry.getName();
                    // 如果是以/开头的
                    if (name.charAt(0) == '/') {
                        // 获取后面的字符串
                        name = name.substring(1);
                    }
                    // 如果前半部分和定义的包名相同
                    if (name.startsWith(packageDirName)) {
                        int idx = name.lastIndexOf('/');
                        // 如果以"/"结尾 是一个包
                        if (idx != -1) {
                            // 获取包名 把"/"替换成"."
                            packageName = name.substring(0, idx).replace('/', '.');
                        }
                        // 如果可以迭代下去 并且是一个包
                        if ((idx != -1) || recursive) {
                            // 如果是一个.class文件 而且不是目录
                            if (name.endsWith(".class") && !entry.isDirectory()) {
                                // 去掉后面的".class" 获取真正的类名
                                String className = name.substring(packageName.length() + 1, name.length() - 6);
                                try {
                                    // 添加到classes
                                    System.out.println("load…" + className);
                                    if (packageName.length() > 0) {
                                        classes.add(loadClass(packageName + '.' + className));
                                    } else {
                                        classes.add(loadClass(className));
                                    }

                                } catch (ClassNotFoundException e) {
                                    // 找不到此类的.class文件");
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
                // log.error("在扫描用户定义视图时从jar包获取文件出错");
                e.printStackTrace();
            }
        }

        return classes;
    }
    /**
     * 以文件的形式来获取包下的所有Class
     *
     * @param packageName
     * @param packagePath
     * @param recursive
     * @param classes
     */
    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath, final boolean recursive,
                                                        Set<Class<?>> classes) {
        // 获取此包的目录 建立一个File
        File dir = new File(packagePath);
        // 如果不存在或者 也不是目录就直接返回
        if (!dir.exists() || !dir.isDirectory()) {
            // log.warn("用户定义包名 " + packageName + " 下没有任何文件");
            return;
        }
        // 如果存在 就获取包下的所有文件 包括目录
        File[] dirfiles = dir.listFiles(new FileFilter() {
            // 自定义过滤规则 如果可以循环(包含子目录) 或则是以.class结尾的文件(编译好的java类文件)
            @Override
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        // 循环所有文件
        for (File file : dirfiles) {
            // 如果是目录 则继续扫描
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive,
                        classes);
            } else {
                // 如果是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    // 添加到集合中去
                    // classes.add(Class.forName(packageName + '.' +
                    // className));
                    // 经过回复同学的提醒，这里用forName有一些不好，会触发static方法，没有使用classLoader的load干净
                    classes.add(
                            Thread.currentThread().getContextClassLoader().loadClass(packageName + '.' + className));
                } catch (ClassNotFoundException e) {
                    // log.error("添加用户自定义视图类错误 找不到此类的.class文件");
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * 加载一个子模块覆盖的类
     *
     * @param name
     * @return
     * @throws ClassNotFoundException
     */
    private Class<?> loadClassForOverriding(String name) throws ClassNotFoundException {
        //查找已加载的类
        Class<?> result = findLoadedClass(name);
        if (result == null) {
            //加载类
            result = findClass(name);
        }
        return result;
    }

    /**
     * 判断该名字是否是需要覆盖的 class
     * @param name
     * @return
     */
    private boolean isEligibleForOverriding(final String name) {
        checkNotNull(name, "name is null");
        return !isExcluded(name) && any(overridePackages, new Predicate<String>() {
            @Override
            public boolean apply(String prefix) {
                return name.startsWith(prefix);
            }
        });
    }

    /**
     * 判断class是否排除
     *
     * @param className
     * @return
     */
    protected boolean isExcluded(String className) {
        checkNotNull(className, "className is null");
        for (String packageName : this.excludedPackages) {
            if (className.startsWith(packageName)) {
                return true;
            }
        }
        return false;
    }

}
