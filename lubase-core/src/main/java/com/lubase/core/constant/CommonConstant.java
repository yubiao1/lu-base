package com.lubase.core.constant;

public class CommonConstant {
    /**
     * 后端应用的ID
     */
    public static final Long SYSTEM_APP_ID = 671085014334574592L;

    /**
     * 管理员导航菜单的根节点ID
     */
    public static final Long ADMIN_NAV_ROOT_ID = 2022052921146766921L;
    /**
     * 设置导航菜单的根节点ID
     */
    public static final Long SETTING_NAV_ROOT_ID = 2022062921146766222L;
    /**
     * 流程引擎应用的ID
     */
    public static final Long WORKFLOW_APP_ID = 769255580404551680L;
    /**
     * JWT sign key
     */
    public static final String JWT_SECRET_KEY = "qiBao";

    /**
     * 超级管理员不可见的菜单
     */
    public static final String SUPPER_UNACCESS_PAGE = "696764753543958528,696765082218008576,696766588694564864,698535459466252288,698585799922290688";
    /**
     * 页签页面的模板代码
     */
    public static final String TAB_PAGE_MASTER_CODE = "1130";


}
