package com.lubase.core.constant;

public class CacheRightConstant {

    /**
     * 超级管理员的角色id
     */
    public static final Long SUPPER_ADMINISTRATOR_ROLE_ID = 688165509325131776L;
    /**
     * 应用管理员角色id
     */
    public static final Long APP_ADMINISTRATOR_ROLE_ID = 695525537208078336L;

    /**
     * 应用配置管理员
     */
    public static final Long APP_SETTING_ROLE_ID = 695525537208078337L;

    public static final String CACHE_NAME_USER_RIGHT = "userRight";
    /**
     * 码表
     */
    public static final String CACHE_NAME_CODE_DATA = "codeData";
    /****************/
    public static final String PRE_PAGE_ADMIN = "'page:admin:671085014334574592'";
    public static final String PRE_PAGE_SETTING = "'page:setting:671085014334574592'";
    public static final String PRE_PAGE_APP = "'page:'";
    /**
     * 获取应用的所属人
     */
    public static final String PRE_APP_MANAGER = "'app:m:'";
    /***************/
    /**
     * 角色功能权限列表
     */
    public static final String PRE_ROLE_FUNC = "'role:func:'";
    /**
     * 获取用户的角色列表
     */
    public static final String PRE_USER_ROLE = "'user:role:'";
    public static final String PRE_USER_APP = "'user:app:'";
    /**
     * 用户登录成功后的权限信息集合
     */
    public static final String PRE_USER_RIGHT_INFO = "'user:rightInfo:'";

    public static final String PRE_ROLE_INFO = "'role:info:'";

    /*********************/
    /**
     * 用户页面级别个性化配置
     */
    public static final String PRE_PERSONALIZATION = "'u:p'";

    /*********************/
    /**
     * 码表
     */
    public static final String PRE_CODE = "'code:'";
    public static final String PRE_CODE_INFO = "'code:info:'";

    /**
     * 用户登录限制次数
     */
    public static final String PRE_USER_LOGIN_ERR = "'user:login:err：'";
    /**
     * 用户登录限制次数
     */
    public static final String PRE_USER_LOGIN_VC = "'user:login:vc：'";

}
